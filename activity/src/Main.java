import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = myObj.nextLine();
        System.out.println("Last Name:");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = myObj.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = myObj.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = myObj.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ". Your grade average is: " + ((firstSubject + secondSubject + thirdSubject) / 3));
    }
}